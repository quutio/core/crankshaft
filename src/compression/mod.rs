pub mod zip;

pub mod stub {
    use serde::Deserialize;
    use serde::Serialize;
    use std::path::PathBuf;

    #[derive(Debug, Clone, Serialize, Deserialize)]
    pub struct Compression {
        pub write_location: PathBuf,
        pub override_glob: Option<String>,
    }
}
