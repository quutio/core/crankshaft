use std::fs::File;
use std::io::{Read, Write};

use ignore::overrides::OverrideBuilder;
use tracing::debug;
use zip::CompressionMethod;
use zip::write::FileOptions;

use crate::Context;

pub fn run(ctx: &Context) -> anyhow::Result<()> {
    let stub = &ctx.config.stub;
    let cmpr = &stub.compression.clone().unwrap();

    match cmpr.write_location.parent() {
        None => {}
        Some(parent) => {
            std::fs::create_dir_all(parent)?
        }
    }

    let file = File::create(&cmpr.write_location)?;
    let base_path = &stub.root;

    println!("{:?}", base_path);

    let mut zip = zip::ZipWriter::new(file);
    let options = FileOptions::default()
        .compression_method(CompressionMethod::Bzip2)
        .unix_permissions(0o755);

    let mut builder = OverrideBuilder::new(base_path);
    cmpr.override_glob.as_ref().map(|glob| {
        builder.add(&glob).unwrap();
    });

    let overrides = builder.build()?;

    let mut buffer = Vec::new();
    for entry in ignore::WalkBuilder::new(base_path)
        .overrides(overrides)
        .build()
    {
        let path = entry.unwrap().clone().into_path();
        let name = path.strip_prefix(&base_path)?;

        if path.is_file() {
            debug!("adding file {:?} as {:?} ...", path, name);

            #[allow(deprecated)]
            zip.start_file_from_path(name, options)?;
            let mut f = File::open(path)?;

            f.read_to_end(&mut buffer)?;
            zip.write_all(&*buffer)?;
            buffer.clear();
        } else if !name.as_os_str().is_empty() {
            debug!("adding dir {:?} as {:?} ...", path, name);
            #[allow(deprecated)]
            zip.add_directory_from_path(name, options)?;
        }
    }

    zip.finish()?;
    Ok(())
}
