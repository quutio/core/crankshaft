use git2::{FetchOptions, RemoteCallbacks};
use git2::build::{CheckoutBuilder, RepoBuilder};

use crate::Context;
use crate::git_impl::stub::add_credentials;

pub(crate) fn run(ctx: &Context) -> anyhow::Result<()> {
    let stub = &ctx.config.stub;

    let mut cb = RemoteCallbacks::new();
    let co = CheckoutBuilder::new();
    let mut fo = FetchOptions::new();

    cb = add_credentials(stub, cb);

    let path = &stub.root;

    fo.remote_callbacks(cb);
    RepoBuilder::new()
        .fetch_options(fo)
        .with_checkout(co)
        .branch(&stub.branch)
        .clone(&stub.remote, path)?;

    Ok(())
}
