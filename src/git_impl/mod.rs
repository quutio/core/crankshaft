pub mod repo_clone;
pub mod repo_pull;

pub mod stub {
    use std::path::PathBuf;

    use git2::{Cred, RemoteCallbacks};
    use serde::Deserialize;
    use serde::Serialize;

    use crate::Stub;

    #[derive(Clone, Debug, Serialize, Deserialize)]
    pub struct CredUserpass {
        pub username: String,
        pub passwd: String,
    }

    #[derive(Clone, Debug, Serialize, Deserialize)]
    pub struct CredSSHAgent {
        pub username: String,
    }

    #[derive(Clone, Debug, Serialize, Deserialize)]
    pub struct CredSSHKey {
        pub publickey: Option<PathBuf>,
        pub privatekey: PathBuf,
        pub passphrase: Option<String>,
    }

    pub fn add_credentials<'a>(stub: &'a Stub, mut cb: RemoteCallbacks<'a>) -> RemoteCallbacks<'a> {
        if let Some(cred_passwd) = &stub.cred_userpass {
            cb.credentials(|_url, _username_from_url, _allowed_types| {
                Cred::userpass_plaintext(&cred_passwd.username, &cred_passwd.passwd)
            });
        }

        if let Some(cred_ssh_agent) = &stub.cred_ssh_agent {
            cb.credentials(|_url, _username_from_url, _allowed_types| Cred::ssh_key_from_agent(&cred_ssh_agent.username));
        }

        if let Some(cred_ssh_key) = &stub.cred_ssh_key {
            cb.credentials(|_url, username_from_url, _allowed_types| {
                Cred::ssh_key(
                    &username_from_url.unwrap(),
                    cred_ssh_key.publickey.as_deref(),
                    &cred_ssh_key.privatekey,
                    cred_ssh_key.passphrase.as_deref(),
                )
            });
        }

        cb
    }
}
