use std::path::PathBuf;

use job_scheduler::Schedule;
use structopt::StructOpt;
use tracing::{error, info, Level};

use lib::{Config, UpdateService};

fn read_config(path: Option<PathBuf>) -> std::io::Result<Config> {
    let path = path.unwrap_or("crankshaft_config.toml".into());
    let c = std::fs::read_to_string(path)?;
    Ok(toml::from_str(&c)?)
}

#[derive(StructOpt, Debug)]
pub enum DebugCommand {
    Clone,
    Pull,
    Zip,
}

#[derive(StructOpt, Debug)]
pub struct RunOpt {
    #[structopt(long, short)]
    cron: String,
}

#[derive(StructOpt, Debug)]
pub enum Command {
    Run(RunOpt),
    Debug(DebugCommand),
}

#[derive(StructOpt, Debug)]
pub struct Opt {
    #[structopt(subcommand)]
    pub command: Command,
    #[structopt(long, short)]
    pub config_path: Option<PathBuf>,
}

fn main() {
    tracing_subscriber::fmt()
        .with_max_level(Level::DEBUG)
        .init();

    let opt = Opt::from_args();

    let config = match read_config(opt.config_path) {
        Ok(config) => config,
        Err(e) => {
            error!("An error occurred while reading config: {}", e);
            panic!()
        }
    };

    let update = UpdateService::new(config);

    match opt.command {
        Command::Run(run_opt) => {
            let cron: Schedule = run_opt.cron.parse().unwrap();
            info!("Running with schedule :: {:?}", run_opt.cron);
            match update.run(cron) {
                Ok(_) => {}
                Err(err) => {error!("{}", err)}
            };
        }
        Command::Debug(debug_cmd) => {
            match debug_cmd {
                DebugCommand::Clone => {
                    match update.git_clone() {
                        Ok(_) => {}
                        Err(err) => {error!("{}", err)}
                    };
                }
                DebugCommand::Pull => {
                    match update.git_pull() {
                        Ok(_) => {}
                        Err(err) => {error!("{}", err)}
                    };
                }
                DebugCommand::Zip => {
                    match update.zip() {
                        Ok(_) => {}
                        Err(err) => {error!("{}", err)}
                    };
                }
            }
        }
    }
}
