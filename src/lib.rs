use std::path::PathBuf;
use std::time::Duration;

use job_scheduler::{Job, JobScheduler, Schedule};
use serde::Deserialize;
use serde::Serialize;
use tracing::error;

use crate::compression::stub::Compression;
use crate::git_impl::stub::{CredSSHAgent, CredSSHKey, CredUserpass};

mod compression;
mod git_impl;
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Stub {
    pub root: PathBuf,
    pub remote: String,
    pub branch: String,

    pub compression: Option<Compression>,

    pub cred_userpass: Option<CredUserpass>,
    pub cred_ssh_agent: Option<CredSSHAgent>,
    pub cred_ssh_key: Option<CredSSHKey>,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Config {
    pub stub: Stub,
}

pub struct UpdateService {
    config: Config,
}

pub struct Context<'a> {
    pub config: &'a Config,
}

impl UpdateService {
    pub fn new(config: Config) -> Self {
        Self { config }
    }

    pub fn config(&self) -> &Config {
        &self.config
    }

    pub fn git_clone(&self) -> anyhow::Result<()> {
        git_impl::repo_clone::run(&Context {
            config: &self.config,
        })
    }

    pub fn git_pull(&self) -> anyhow::Result<()> {
        git_impl::repo_pull::run(&Context {
            config: &self.config,
        })
    }

    pub fn zip(&self) -> anyhow::Result<()> {
        compression::zip::run(&Context {
            config: &self.config,
        })
    }

    pub fn run(&self, cron: Schedule) -> anyhow::Result<()> {
        let mut sched = JobScheduler::new();

        if !self.config.stub.root.exists() {
            self.git_clone()?
        }

        sched.add(Job::new(cron, || {
            let _ = match self.git_pull() {
                Ok(_) => {}
                Err(err) => {
                    error!("{}", err)
                }
            };

            match self.config.stub.compression {
                None => {}
                Some(_) => {
                    match self.zip() {
                        Ok(_) => {}
                        Err(err) => {
                            error!("{}", err)
                        }
                    }
                }
            }
        }));

        loop {
            sched.tick();
            std::thread::sleep(Duration::from_millis(500))
        }
    }
}
