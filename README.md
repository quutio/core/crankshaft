# <//> Crankshaft
> A Git poll utility.

A simple utility for polling git remote changes.

## Building

The project can be built with Cargo.

`$` `cargo build --release`
